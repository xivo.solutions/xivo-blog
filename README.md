XiVO blog
=========

This repository has been deprecated and has been replaced by a new blog where sources can be found @xivo-solutions-blog.

--

Content is now hosted [here](https://xivo-solutions-blog.gitlab.io).